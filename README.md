## PubChem Chemical Property Finder
A lightweight tool for querying the
[PubChem REST API](https://pubchemdocs.ncbi.nlm.nih.gov/pug-rest)
for chemical property data (i.e. melting point, boiling point, heat of combustion, etc.).

### Overview
The `property_finder.py` module defines the `PubChemFinder` class which
serves as the primary engine for querying the PubChem API.
The script `find_compound_properties.py` can be run from the command line with
the following arguments to utilize an `PubChemFinder` to
perform a query routine:

### Installation
```
$ git clone git@bitbucket.org:tylerhuntington222/f2ft-property-finder.git
```

### Requirements
An installation of Python 3.6 or later is required to run all code in this
repository. Additionally, the packages (with associated version numbers) listed
in `requirements.txt` must be installed. The easiest way to get up and
running is to create a Python 3.6 virtual environment and install the
dependencies in batch using the following series of commands from within
this repository's root directory:

(The first command is only necessary if you do not have `virtualenv` installed)

```
$ pip install virtualenv
```

```
$ virtualenv --python=python3.6 py36env
```

```
$ source py36env/bin/activate
```

```
$ pip install -r requirements.txt
```

Assuming all these commands run smoothly, you should be set up to run
all the scripts as outlined below. When you are finished working inside this
project repository, you can deactivate the virtual environment
you just created (`py36env`) by running the command:

```
$ deactivate
```

### Usage
(Assuming you have cloned this repo, set up a virtual environment following
the above steps and navigated into the `/src` subdirectory.)

```
$ python find_compound_properties.py [-h] [-s <path>] [-p <path>]
```

###  Command Line Arguments
##### `-h`    `--help`
Show this help message and exit

##### `-s`    `--search_smiles_filepath`
Filepath to CSV file with a column titled
'SMILES' containing the SMILES strings of compounds
to be be queried for properties data. An example
CSV is provided in the `data/` directory of this module's parent
repository with the filename: `cn_model_v2.0_full-ECNet.csv`.
This file is also used as the default if no filepath is provided
for this command line argument.

##### `-p`    `--search_properties_json_filepath`
Filepath to JSON file specifying the PubChem
TOC Headings (which correspond to compound)
properties to be searched for. By default, the file
`search_properties_config.json` in the `config/`
directory  which
includes 'Melting Point', 'Boiling Point' and
'Density' as target properties. Modifying this
file while conforming to its default structure
is the easiest way to specify properties to
be searched for. Any 'leaf'
(i.e. innermost element in the tree which cannot be expanded to expose sub-elements)
in the [PubChem Compound
TOC Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72)
qualifies as a valid element of the `"TOCHeading"`
list in `search_properties_config.json`
(default=`../config/search_properties_config.json`). Note that strings
in `"TOCHeading"` list must *exactly* match (i.e. spelling, case, whitespace)
target leaf nodes of the PubChem Compound TOC Tree.
The contents of the default `search_properties_config.json`file is shown below
for reference:

```JSON
{
  "TOCHeadings": [
    "Melting Point",
    "Boiling Point",
    "Density"
  ]
}
```

##### `-o`    `--output_filepath`
Filepath of output CSV that will be generated containing
records of matched compound properties from the PubChem API.
By default, the output file is written to:
`../query_results/pubchem_property_finder_results.csv`.
Rows in the CSV will be uniquely idenfified SMILES strings
corresponding to those provided in the input CSV. There will
be at most the same number of SMILES strings in the output as
were provided in the input, but likely there will be fewer:
only compounds for which queried properties were found
in the PubChem database are included in the output. If
multiple values for a property (i.e. from different sources) were
found in PubChem, they are included as additional columns in
the output CSV. The column naming convention should be explicit
enough to interpret.
(default = `../query_results/pubchem_property_finder_results.csv`)

##### `-l`    `--query_limit`
Optional limit on number of queries to submit.
Default value of -1 indicates that no limit should be used (i.e
all queries will be run).
(default = -1,)

#### Example Usage
```
$ python find_compound_properties.py -p ../data/dataset_with_smiles_col.csv -o my_pubchem_query_results.csv
```

Note that default values are provided for all command line arguments (see
table above), so executing
```
$ python find_compound_properties.py
```
will run with those defaults.

Running `find_compound_properties.py` will initiate a set of queries to
the PubChem API for compounds that match the SMILES strings provided as
input. Progress bars will appear in the terminal for monitoring the status
of the querying routine. After processing all query responses, the specified
properties of interest that were found for each compound will be compiled and
exported into a CSV file at the filepath provided as a command line argument.

In cases where multiple reported values were retrieved from PubChem for
given compound property, each value is given its own column in the output CSV.
Additionally, the corresponding reference and any units or narrative
description provided. In the property value columns there is still some
parsing to be done due to lack of consistency in records
(i.e. "35 DEG C", "35° C", "35 degrees C", etc.).
At present, this module does not perform any parsing to extract/standardize
these numerical values. Rather they are stored as strings in the unmodified
form as represented in PubChem's database. With this in mind, the
it is advised that users perform manual screening/data cleaning before any
downstream data analyses.
