import argparse
import json
import requests
from abc import ABCMeta, abstractmethod
from datetime import datetime
import pubchempy as pcp
import pandas as pd
from collections import OrderedDict
from tqdm import tqdm
from property_finder import PubChemPropertyFinder

def main():
    # configure command line args
    parser = argparse.ArgumentParser(
        description="""
            A data mining tool for collecting compound properties
            from the PubChem REST API.
        """
    )

    parser.add_argument(
        '--search_smiles_csv_filepath',
        '-s',
        # default='../data/cn_model_v2.0_full-ECNet.csv',
        default='../data/BradleyMeltingPointDataset.csv',
        help="""
            Filepath to CSV file with a column titled
            'SMILES' containing the SMILES strings of compounds
            to be be queried for properties data. An example
            CSV is provided in the ../data directory of this module's parent
            repository with the filename: `cn_model_v2.0_full-ECNet.csv`.
            This file is also used as the defulat if no filepath is provided
            as a command line argument.
        """
    )

    parser.add_argument(
        '--search_params_json_filepath',
        '-p',
        default='../config/search_params_config.json',
        help="""
            Filepath to JSON file specifying the PubChem
            TOC Headings (which correspond to compound)
            properties that should be used to filter
            search results. By default, the file
            'search_params_config.json' in `../.config`
            directory of this module's parent repository which
            includes 'Melting Point', 'Boiling Point' and
            'Density' as target properties. Modifying this
            file while conforming to its default structure
            is the easiest way to specify properties to
            be searched for. Any 'leaf' in the PubChem Compound
            TOC Tree found here:
            https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72
            qualifies as a valid element of the "TOCHeading"
            list in `search_params_config.json`
        """
    )

    parser.add_argument(
        '--output_filepath',
        '-o',
        default='../query_results/pubchem_property_finder_results.csv',
        help="""
            Filepath of output CSV that will be generated containing
            records of matched compound properties from the PubChem API.
            By default, the output file is written to:
            `../query_results/pubchem_property_finder_results.csv`.
            Rows in the CSV will be uniquely idenfified SMILES strings
            corresponding to those provided in the input CSV. There will
            be at most the same number of SMILES strings in the output as
            were provided in the input, but likely there will be fewer:
            only compounds for which queried properties were found
            in the PubChem database are included in the output. If
            multiple values for a property (i.e. from different sources) were
            found in PubChem, they are included as additional columns in
            the output CSV. The column naming convention should be explicit
            enough to interpret.
        """
    )
    parser.add_argument(
        '--query_limit',
        '-l',
        default=-1,
        help="""
            Optional limit on number of queries to submit.
            Default value of -1 indicates that no limit should be used.
        """
    )

    # parse command line args
    args = parser.parse_args()
    smiles_csv_fp = args.search_smiles_csv_filepath
    search_params_json_fp = args.search_params_json_filepath
    outfile_fp = args.output_filepath
    limit = int(args.query_limit)
    if limit == -1:
        limit = None

    # init PropertyFinder
    finder = PubChemPropertyFinder()

    # load SMILES strings to search for
    print('\nLoading CSV with SMILES strings of target compounds...')
    finder.load_smiles_list_from_csv(
        csv_fp=smiles_csv_fp, smiles_col='smiles'
    )
    print(f'{len(finder.smiles_strings)} '\
          'SMILES strings successfully loaded from CSV.')

    # load the TOCHeadings to be searched for
    print(search_params_json_fp)

    # finder.load_TOCHeadings_from_json(search_params_json_fp)

    finder.load_target_properties_from_json(search_params_json_fp)

    print('\nSearch properties config JSON loaded.')
    print('Compounds will be queried for: \n' \
          f"{finder.TOCHeadings}")

    finder.search_for_props_by_smiles(lim=limit)
    # finder.filtered_pug_view_records_to_csv(outfile_fp)
    finder.pug_rest_smiles_responses_to_csv(outfile_fp)

if __name__ == '__main__':
    main()
