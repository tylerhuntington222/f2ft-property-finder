import argparse
import json
import requests
from abc import ABCMeta, abstractmethod
from datetime import datetime
import pubchempy as pcp
import pandas as pd
import numpy as np
from collections import OrderedDict


class PropertyFinder(metaclass=ABCMeta):
    """
    Base class for property collection utility class.
    """

    def __init__(self):
        pass

    @abstractmethod
    def search_for_props_by_smiles(self, smiles):
        """
        Searches for compounds by SMILES strings.
        """
        pass


class PubChemPropertyFinder(PropertyFinder):
    """
    Subclass of PropertyFinder for gathering compound property data from
    Pub Chem's API.
    """

    def __init__(self):
        super().__init__()
        self.TOCHeadings = []
        self.target_properties = []

    def load_smiles_list_from_csv(self, csv_fp, smiles_col):
        try:
            df = pd.read_csv(csv_fp)
        except FileNotFoundError as e:
            exit('\nError: No valid CSV file with SMILES strings ' 
                  'found at provided filepath.\n')
        df.columns = [c.lower() for c in df.columns]
        self.smiles_strings = list(df[smiles_col.lower()])

    def search_for_props_by_smiles(
        self, smiles=None, TOCHeadings=[], target_properties=[], lim=None,
            cache_results=False):
        """

        """
        if smiles:
            self.smiles_strings = smiles

        if lim is None:
            lim = len(self.smiles_strings)

        if TOCHeadings == []:
            TOCHeadings = self.TOCHeadings

        if target_properties == []:
            target_properties = self.target_properties


        self.pug_rest_records = {}
        print("\nQuerying PUG REST API for compounds "
              "with input SMILES strings...")
        t = len(self.smiles_strings) if len(self.smiles_strings) > lim else lim
        i = 0
        log_rate = 1
        for s in self.smiles_strings[:lim]:
            # print status update
            if i % log_rate == 0:
                print(f'{i}/{t} Queries Complete...')
            i += 1
            try:
                cmpds = pcp.get_compounds(s, 'smiles')
                self.pug_rest_records[s] = []
                for c in cmpds:
                    if c.record['id'] == {}:
                        print(f'No existing compounds found for SMILES: {s}')
                    self.pug_rest_records[s].append(c.record)
            except pcp.BadRequestError as e:
                print(e)
                print(f'Error occurred while querying for SMILES: {s}')


        if cache_results:
            ts = str(datetime.now())[:-10].replace(' ', '_').replace(':', '.')
            fp = f'../query_results/pug_rest_query_results_{ts}.json'
            with open(fp, 'w') as f:
                json.dump(self.pug_rest_records, f, indent=2)

        if TOCHeadings:
            self.query_pug_view_by_cids()
            self.filter_pug_view_records_for_headings(TOCHeadings)

        if target_properties:
            self.query_pug_rest_by_cids(target_properties=target_properties)


    def _rec_heading_search(self, root, TOCHeadings):
        """
        Recursive generator for traversing a nested dict which may contain
        sub-records that match target PubChem TOC Headings.

        Args:
        ---
            root (dict): top-level dictionary of a nested dict that should
                be searched for sub-records matched by elements of TOCHeadings.
            TOCHeadings (list of str): Exact names of TOC Headings to search
                for in compound records. Any 'leaf' in the PubChem Compound
                TOC Tree found here:
                https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72
                qualifies as a valid element of this list.

        Yields:
        ___
            Nested dictionaries within `root` in which the value of the
            'TOCHeading' key is exactly matched by a search term in the
            TOCHeadings list passed by caller.

        """
        if isinstance(root, dict):
            for k in root:
                if k == 'TOCHeading' and root[k] in TOCHeadings:
                    yield root
                if isinstance(root[k], dict):
                    for res in self._rec_heading_search(root[k], TOCHeadings):
                        yield res
                if isinstance(root[k], list):
                    for elem in root[k]:
                        for res in self._rec_heading_search(elem, TOCHeadings):
                            yield res

    def filter_pug_view_records_for_headings(self, TOCHeadings):
        """
        Filters a dict of PUG View records (in which keys are SMILES strings
        and values are the response objects--represented as dicts---from
        corresponding PUG View queries for specified target PubChem TOC
        Headings.

        Args:
        ---
            TOCHeadings (list of str): Exact names of TOC Headings to search
                for in compound records. Any 'leaf' in the PubChem Compound
                TOC Tree found here:
                https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72
                qualifies as a valid element of this list.

        Returns:
        ___
            Filters a dict of PUG View records (in which keys are
            SMILES strings and values are the corresponding response
            objects--represented as dicts---from PUG View API for
            filtered for PubChem TOC Headings exactly matched by
            search terms in the TOCHeadings list passed by caller.
        """
        res_dict = {}
        for smiles in self.pug_view_records:
            cmpd_dict = {}
            gen = self._rec_heading_search(
                self.pug_view_records[smiles], TOCHeadings
            )
            for item in gen:
                heading = item['TOCHeading']
                cmpd_dict[heading] = item.copy()
                cmpd_dict[heading].pop('TOCHeading')
                ref = self.pug_view_records[smiles]['Record']['Reference']
                cmpd_dict['Reference'] = ref
            if cmpd_dict == {}:
                # print(f'None of queried properties found for: {smiles}')
                pass
            else:
                res_dict[smiles] = cmpd_dict
        print(f"\nAt least one of {TOCHeadings} found for " \
              f'{len(res_dict)}/{len(self.pug_view_records)} compounds\n')

        self.filtered_pug_view_recs = res_dict

    @staticmethod
    def standardize_smiles_strings(input_smiles, lim=None):
        """
        Args:
            input_smiles (list of str): input SMILES strings to be standandized.
        """
        res = {}
        print("\nQuerying PUG REST API for compounds "
              "with input SMILES strings...")
        lim = lim if lim else len(input_smiles)
        i = 0
        log_rate = 1
        for s in input_smiles[:lim]:
            # print status update
            if i % log_rate == 0:
                print(f'{i}/{lim} Queries Complete...')
            i += 1
            res[s] = {}
            try:
                cmpds = pcp.get_compounds(s, 'smiles')
                if len(cmpds) > 0:
                    if len(cmpds) > 1:
                        print(f'More than one PubChem result for SMILES: {s}')
                    c = cmpds[0]
                    res[s]['canonical_smiles'] = c.canonical_smiles \
                        if c.canonical_smiles else np.nan
                    res[s]['isomeric_smiles'] = c.isomeric_smiles \
                        if c.isomeric_smiles else np.nan
            except pcp.BadRequestError as e:
                print(e)
                print(f'Error occurred while querying for SMILES: {s}')
                res[s]['canonical_smiles'] = np.nan
                res[s]['isomeric_smiles'] = np.nan

            except json.decoder.JSONDecodeError as e:
                print(e)
                print(f'Bad JSON returned by query for SMILES:: {s}')
                res[s]['canonical_smiles'] = np.nan
                res[s]['isomeric_smiles'] = np.nan

            except Exception as e:
                print(e)
                print(f'Uncaught exception while querying for SMILES: {s}')
                res[s]['canonical_smiles'] = np.nan
                res[s]['isomeric_smiles'] = np.nan

        df = pd.DataFrame.from_dict(res, orient='index')
        df['input_smiles'] = list(df.index)
        df.reset_index(drop=True, inplace=True)
        return df

    @staticmethod
    def query_pc_for_smiles_by_cmpd_names(cmpd_names,
                                          cmpd_syns, lim=None):
        """
        Args:
            cmpd_names (list of str): primary compound names to
                be used for querying PubChem to obtain a SMILES string.
            cmpd_syns (list of lists of str): strings of compound
                synonyms to be used for querying if primary compound name
                does not return matches.
        """
        res = {}
        print("\nQuerying PUG REST API for compounds "
              "with input SMILES strings...")
        lim = lim if lim else len(cmpd_names)
        i = 0
        log_rate = 1
        # TODO: remove temp variable `start_rec` used for debugging
        start_rec = 0
        for i in range(start_rec, len(cmpd_names))[:lim]:
            name = cmpd_names[i]
            syns = cmpd_syns[i]
            # print status update
            if i % log_rate == 0:
                print(f'{i}/{lim} Queries Complete...')
            i += 1
            res[name] = {}
            if name == '' and syns == []:
                print('No name or synonyms for this compound.')
                continue
            try:
                print(f'Querying for compound: {name}')
                cmpds = pcp.get_compounds(name, namespace='name')
                if len(cmpds) == 0:
                    print(f'No results for name: {name}')
                    for s in syns:
                        print(f'Querying for synonym: {s}')
                        cmpds = pcp.get_compounds(s, 'name')
                        if len(cmpds) > 0:
                            break
                if len(cmpds) > 0:
                    if len(cmpds) > 1:
                        print(f'More than one PubChem result for: {name}')
                    c = cmpds[0]
                    res[name]['canonical_smiles'] = c.canonical_smiles \
                        if c.canonical_smiles else np.nan
                    res[name]['isomeric_smiles'] = c.isomeric_smiles \
                        if c.isomeric_smiles else np.nan
                # handle case where no matches for primary name or synonyms
                else:
                    print(f'No results for {name} or its synonyms:')
                    for s in syns:
                        print(f'\t{s}')
                    res[name]['canonical_smiles'] = np.nan
                    res[name]['isomeric_smiles'] = np.nan

            except pcp.BadRequestError as e:
                print(e)
                print(f'Error occurred while querying for: {name}')
                res[name]['canonical_smiles'] = np.nan
                res[name]['isomeric_smiles'] = np.nan

            except json.decoder.JSONDecodeError as e:
                print(e)
                print(f'Bad JSON returned by query for:: {name}')
                res[name]['canonical_smiles'] = np.nan
                res[name]['isomeric_smiles'] = np.nan

            except Exception as e:
                print(e)
                print(f'Uncaught exception while querying for: {name}')
                res[name]['canonical_smiles'] = np.nan
                res[name]['isomeric_smiles'] = np.nan

        df = pd.DataFrame.from_dict(res, orient='index')
        df['cmpd_name'] = list(df.index)
        df.reset_index(drop=True, inplace=True)
        return df

    def pug_rest_smiles_responses_to_df(self):
        res = {}
        for in_smiles in self.pug_rest_records.keys():
            resp = self.pug_rest_records[in_smiles]
            res[in_smiles] = {}
            res[in_smiles]['isomeric_smiles'] = \
                resp['PropertyTable']['Properties'][0]['IsomericSMILES']
            res[in_smiles]['canonical_smiles'] = \
                resp['PropertyTable']['Properties'][0]['CanonicalSMILES']
        df = pd.DataFrame.from_dict(res, orient='index')
        df['input_smiles'] = list(df.index)
        df.reset_index(drop=True, inplace=True)
        return df

    def pug_rest_smiles_responses_to_csv(self, outfile_fp):
        df = pug_rest_smiles_responses_to_df()
        df.to_csv(outfile_fp, index=False)

    def filtered_pug_view_records_to_df(self, pug_view_recs=None):
        if pug_view_recs == None:
            pug_view_recs = self.filtered_pug_view_recs
        # init empty of dicts which will become rows (where keys=column names)
        # in the dataframe generated by this function
        recs_accum = []
        for smiles in pug_view_recs:
            refs_list = pug_view_recs[smiles]['Reference']
            rec = OrderedDict()
            rec['SMILES'] = smiles
            prop = ''
            for prop in pug_view_recs[smiles]:
                if prop == 'Reference':
                    continue
                # init counter for number of different sources that report
                # a given property
                n = 1

                if 'Information' not in pug_view_recs[smiles][prop]:
                    continue
                # iterate over values for a particular property
                # reported by different sources
                for entry in pug_view_recs[smiles][prop]['Information']:
                    val = entry['Value']
                    if type(val) == dict:
                        for key in val:
                            if key == 'Number':
                                val = str(val[key][0])
                            elif key == 'StringWithMarkup':
                                val = val[key][0]['String']
                            # try to parse the units from 'Units' field
                            # if it exists for this entry
                            if 'Units' in val:
                                units = val['Units']
                            else:
                                units = None
                            # try to parse description from 'Description'
                            # field if it exists for this entry
                            if 'Description' in val:
                                units = val['Description']
                            else:
                                desc = None

                    # define prefix for columns that will correspond to
                    # reported values from the current source
                    base_colname = f"{prop.replace(' ', '_')}_source{n}"
                    n += 1
                    rec[f'{base_colname}_value'] = val
                    rec[f'{base_colname}_units'] = units
                    rec[f'{base_colname}_description'] = desc

                    # get reference info for this reported value
                    # and add string representation of JSON into
                    # a column with the _reference suffix
                    ref_num = entry['ReferenceNumber']
                    for ref in refs_list:
                        if ref['ReferenceNumber'] == ref_num:
                            ref_str = json.dumps(ref)
                    rec[f'{base_colname}_reference'] = ref_str
            recs_accum.append(rec)

        return pd.DataFrame(recs_accum)


    def filtered_pug_view_records_to_csv(self, fp, df=None):
        if df == None:
            df = self.filtered_pug_view_records_to_df()

        df.to_csv(fp, index=False)
        print(f"\nResults exported to CSV with relative filepath:\n{fp}\n")


    def query_pug_view_by_cids(self, filter=None,
                               lim=None, res_format='JSON',
                               cache_results=False):

        if not lim:
            lim = len(self.pug_rest_records)

        api_prefix = 'https://pubchem.ncbi.nlm.nih.gov/rest/pug_view/data'

        self.pug_view_records = {}

        smiles_strings = list(self.pug_rest_records.keys())
        print(f'\nQuerying PUG View API for matching compound CIDs..')
        for smiles in smiles_strings[:lim]:
            try:
                cid = self.pug_rest_records[smiles][0]['id']['id']['cid']
                url = f'{api_prefix}/compound/{cid}/{res_format}'
                print(f'\nQuery URL: {url}\n')
                rv = requests.get(url=url)
                data = rv.json()
                self.pug_view_records[smiles] = data

            except KeyError:
                print(f'No existing compounds found for SMILES: {smiles}')

        if cache_results:
            ts = str(datetime.now())[:-10].replace(' ', '_').replace(':', '.')
            fp = f'../query_results/pug_view_query_results_{ts}.json'
            with open(fp, 'w') as f:
                json.dump(self.pug_view_records, f, indent=2)

        # filtered_res = {}
        # if filter != None:
        #     for smiles in self.pug_view_records:
        #         filtered_res[smiles] = {}
        #         cid = self.pug_view_records[smiles]['RecordNumber']
        #         filtered_res[smiles]['cid'] = cid
        #         for section in pug_view_records[smiles]['Section']:
        #             heading = section['TOCHeading']

    def query_pug_rest_by_cids(self, target_properties=None,
                               lim=None, res_format='JSON',
                               cache_results=False):

        if not lim:
            lim = len(self.pug_rest_records)

        api_prefix = 'https://pubchem.ncbi.nlm.nih.gov/rest/pug/'

        smiles_strings = list(self.pug_rest_records.keys())
        target_properties = ','.join(target_properties)
        prev_pug_rest_records = dict(self.pug_rest_records)
        self.pug_rest_records = {}
        print(f'\nQuerying PUG View API for matching compound CIDs..')
        for smiles in smiles_strings[:lim]:
            try:
                cid = prev_pug_rest_records[smiles][0]['id']['id']['cid']
                if target_properties:
                    url = f'{api_prefix}/compound/cid/{cid}/property/' \
                        f'{target_properties}/{res_format}'
                else:
                    url = f'{api_prefix}/compound/{cid}/{res_format}'
                print(f'\nQuery URL: {url}\n')
                rv = requests.get(url=url)
                data = rv.json()
                self.pug_rest_records[smiles] = data

            except KeyError:
                print(f'No existing compounds found for SMILES: {smiles}')

        if cache_results:
            ts = str(datetime.now())[:-10].replace(' ', '_').replace(':', '.')
            fp = f'../query_results/pug_view_query_results_{ts}.json'
            with open(fp, 'w') as f:
                json.dump(self.pug_view_records, f, indent=2)

        # filtered_res = {}
        # if filter != None:
        #     for smiles in self.pug_view_records:
        #         filtered_res[smiles] = {}
        #         cid = self.pug_view_records[smiles]['RecordNumber']
        #         filtered_res[smiles]['cid'] = cid
        #         for section in pug_view_records[smiles]['Section']:
        #             heading = section['TOCHeading']

    def load_pug_rest_records_from_json(self, json_fp):
        with open(json_fp, 'r') as f:
            self.pug_rest_records = json.load(f)

    def load_TOCHeadings_from_json(self, json_fp):
        with open(json_fp, 'r') as f:
            rv = json.load(f)
        self.TOCHeadings = rv['TOCHeadings']

    def load_target_properties_from_json(self, json_fp):
        """
        Loads list of user-specified compound properties that should be
        returned in responses from the PUGRest API. Valid property names
        are given under the "Compound Properties Tables" heading
        in the PUGRest docs: https://pubchemdocs.ncbi.nlm.nih.gov/pug-rest
        """
        with open(json_fp, 'r') as f:
            rv = json.load(f)
        self.target_properties = rv['Properties']

    def _get_cids_from_pug_rest_records(self):
        cids = {}
        for smiles in self.pug_rest_records.keys():
            try:
                cid = self.pug_rest_records[smiles][0]['id']['id']['cid']
                cids[smiles] = cid
            except KeyError:
                print(f'No existing compounds found for SMILES: {smiles}')


def main():
    # configure command line args
    parser = argparse.ArgumentParser(
        description="""
            A data mining tool for collecting compound properties
            from the PubChem REST API.
        """
    )

    parser.add_argument(
        '--search_smiles_csv_filepath',
        '-s',
        default='../data/cn_model_v2.0_full-ECNet.csv',
        help="""
            Filepath to CSV file with a column titled
            'SMILES' containing the SMILES strings of compounds
            to be be queried for properties data. An example
            CSV is provided in the ../data directory of this module's parent
            repository with the filename: `cn_model_v2.0_full-ECNet.csv`.
            This file is also used as the defulat if no filepath is provided
            as a command line argument.
        """
    )

    parser.add_argument(
        '--search_properties_json_filepath',
        '-p',
        default='../config/search_properties_config.json',
        help="""
            Filepath to JSON file specifying the PubChem
            TOC Headings (which correspond to compound)
            properties that should be used to filter
            search results. By default, the file
            'search_properties_config.json' in `../.config`
            directory of this module's parent repository which
            includes 'Melting Point', 'Boiling Point' and
            'Density' as target properties. Modifying this
            file while conforming to its default structure
            is the easiest way to specify properties to
            be searched for. Any 'leaf' in the PubChem Compound
            TOC Tree found here:
            https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72
            qualifies as a valid element of the "TOCHeading"
            list in `search_properties_config.json`
        """
    )

    parser.add_argument(
        '--output_filepath',
        '-o',
        default='../query_results/pubchem_property_finder_results.csv',
        help="""
            Filepath of output CSV that will be generated containing
            records of matched compound properties from the PubChem API.
            By default, the output file is written to:
            `../query_results/pubchem_property_finder_results.csv`.
            Rows in the CSV will be uniquely idenfified SMILES strings
            corresponding to those provided in the input CSV. There will
            be at most the same number of SMILES strings in the output as
            were provided in the input, but likely there will be fewer:
            only compounds for which queried properties were found
            in the PubChem database are included in the output. If
            multiple values for a property (i.e. from different sources) were
            found in PubChem, they are included as additional columns in
            the output CSV. The column naming convention should be explicit
            enough to interpret.
        """
    )
    parser.add_argument(
        '--query_limit',
        '-l',
        default=-1,
        help="""
            Optional limit on number of queries to submit.
            Default value of -1 indicates that no limit should be used.
        """
    )

    # parse command line args
    args = parser.parse_args()
    smiles_csv_fp = args.search_smiles_csv_filepath
    search_props_json_fp = args.search_properties_json_filepath
    outfile_fp = args.output_filepath
    limit = int(args.query_limit)
    if limit == -1:
        limit = None

    # init PropertyFinder
    finder = PubChemPropertyFinder()
    fp = '../data/cn_model_v2.0_full-ECNet.csv'
    print('\nLoading CSV with SMILES strings of target compounds...')
    finder.load_smiles_list_from_csv(
        csv_fp=smiles_csv_fp, smiles_col='SMILES'
    )
    print(f'{len(finder.smiles_strings)} '\
          'SMILES strings successully loaded from CSV.')

    # load search smiles
    # load the TOCHeadings to be searched for
    print(search_props_json_fp)
    finder.load_TOCHeadings_from_json(search_props_json_fp)

    print('\nSearch properties config JSON loaded.')
    print('Compounds will be queried for: \n' \
          f"{finder.TOCHeadings}")

    finder.search_for_props_by_smiles(lim=limit)
    finder.filtered_pug_view_records_to_csv(outfile_fp)

if __name__ == '__main__':
    main()
