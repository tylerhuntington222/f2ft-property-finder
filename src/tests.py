import json
import unittest
from property_finder import PubChemPropertyFinder

class TestPubChemPropertyFinder(unittest.TestCase):
    def setUp(self):
        self.finder = PubChemPropertyFinder()

    def test_load_smiles_list_from_csv(self):
        fp = '../data/cn_model_v2.0_full-ECNet.csv'
        self.finder.load_smiles_list_from_csv(csv_fp=fp, smiles_col='SMILES')
        self.assertTrue(True)

    def test_search_for_props_by_smiles(self):
        fp = '../data/cn_model_v2.0_full-ECNet.csv'
        self.finder.load_smiles_list_from_csv(csv_fp=fp, smiles_col='SMILES')
        self.finder.search_for_props_by_smiles(lim=10)
        self.assertTrue(True)

    def test_load_pug_rest_records_from_json(self):
        json_fp = '../query_results/res.json'
        self.finder.load_pug_rest_records_from_json(json_fp)
        self.finder.query_pug_view_by_cids()
        self.assertTrue(True)

    def test_filter_pug_view_records_for_headings(self):
        fp = '../query_results/pug_view_query_results_2019-05-24_14:26.json'
        with open(fp, 'r') as f:
            root = json.load(f)
        TOCHeadings = ['Melting Point', 'Boiling Point', 'Density']
        self.finder = PubChemPropertyFinder()
        self.finder.pug_view_records = root
        self.finder.filter_pug_view_records_for_headings(TOCHeadings)
        self.assertTrue(True)

    def test_filtered_pug_view_records_to_df(self):
        fp = '../query_results/pug_view_query_results_2019-05-24_14:26.json'
        with open(fp, 'r') as f:
            root = json.load(f)
        TOCHeadings = ['Melting Point', 'Boiling Point', 'Density']
        self.finder = PubChemPropertyFinder()
        self.finder.pug_view_records = root
        filtered_recs = self.finder.filter_pug_view_records_for_headings(
            TOCHeadings
        )
        self.finder.filtered_pug_view_records_to_df(filtered_recs)
        self.assertTrue(True)

    def test_filtered_pug_view_records_to_csv(self):
        fp = '../query_results/pug_view_query_results_2019-05-24_14:32.json'
        with open(fp, 'r') as f:
            root = json.load(f)
        TOCHeadings = ['Melting Point', 'Boiling Point', 'Density']
        self.finder = PubChemPropertyFinder()
        self.finder.pug_view_records = root
        filtered_recs = self.finder.filter_pug_view_records_for_headings(
            TOCHeadings
        )
        res = self.finder.filtered_pug_view_records_to_df(filtered_recs)
        self.finder.filtered_pug_view_records_to_csv(res, fp='test_output.csv')
        self.assertTrue(True)

if __name__ == '__main__':
    unittest.main()
